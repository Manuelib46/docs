# PLATEBCO-SERVICE  - Deployment Manual

## Tabla de contenido

 - [PLATEBCO-SERVICE - Deployment Manual ](#Ejecucion---Deployment-Manual)

	 -[Tabla de contenido](#tabla-de-contenido)

	 -[Archivos Necesarios ](#archivos-necesarios)

	 -[¿Cómo Ejecutar el Script?](#cómo-ejecutar-el-script)

	 -[Proceso durante la instalacion](#proceso-durante-la-instalacion)

## Archivos Necesarios
Como primer paso recuerde que los archivos **Anaconda3-2020.07-Windows-x86_64.EXE**  , **SearchChannels-master. zip** y los archivos con extensión **.PS1** deben estar dentro de la carpeta **Documentos** la cual se puede encontrar como se muestra en la siguiente imagen

![Documentos](img/0.jpg)


## Cómo ejecutar el script?

Para ejecutar el script recuerde que los archivos deben estar dentro de la carpeta **Mis Documentos** .
1. Como Primer paso ubique el archivo **instalación.bat**

![Documentos](img/1.png)

2. click derecho al archivo **instalación.bat** y buscar la opcion **Ejecutar como Administrador**

![instalador](img/2.jpg)

3. **Nota** Recuerde que el archivo esta automatizado para realizar el proceso de instalación


## Proceso durante la instalacion

Durante el proceso de instalacion windows abrira una ventana como se muestra en la siguiente imagen

![Documentos](img/3.png)

A continuación en esta ventana se irá notificando el proceso que está ejecutando durante la instalación:

1. Se verificara si **Anaconda** se encuentra instalado

	![Documentos](img/4.png)

2. Debido a que **Anaconda** no esta instalado iniciará el proceso de instalación	

	![instalacion Anaconda](img/5.png)

	**Nota: Por favor sea paciente durante este paso ya que el proceso de instalación lleva tiempo**

3. Una vez finalizado de instalar Anaconda se inciará a extraer el archivo **SearchChannels-master.zip** en la carpeta **Documentos** y quedara como se ve en la siguiente imagen
	
	![Extraccion zip](img/6.png)

4. Despúes de extraer el archivo zip se guardaran las rutas de instalacion para la ejecucion de los programas anteriormente instalados, mostrara un mensaje como el siguiente 

	![variables de sistema](img/7.png)

5. Una vez agregadas las variables de sistema sera notificando del proceso

	![variables de sistema](img/8.png)

6. Se verificara si se encuentra instalado **PM2**, debido a que no esta instalado empezara el proceo de instalación y se mostrara un mensaje como el siguiente

	![variables de sistema](img/9.png)

7. Una vez instalado PM2 le mostrara el siguiente mensaje

	![PM2](img/10.png)

8. Uno de los siguientes pasos que le notificará será la creación de las carpetas **Log** y **Download**  y las verá creadas respectivamente

	![Carpetas log ](img/11.png)

	![Carpeta download](img/12.png)

9. El programa le notificara cuando se este creando el **entorno**

	![Creando entorno](img/4.jpg)

10. Una vez creado el **entorno** será notificando

	![Entorno creado](img/5.jpg)

11. El programa seleccionará el **entorno** que se acaba de crear para instalar las **dependencias**

	![Entorno seleccionado](img/6.jpg)

12. Le ira notificando la instalación de **dependencias**

	![Instalacion dependencias](img/8.jpg)

13. Se iniciará el servicio por **PM2**

	![Inicio de servicio](img/9.jpg)

14. Se creara un servicio de windows que siga en funcionamiento

	![Servicio Windows](img/10.jpg)

15. Se crea automaticamente un proceso para que **inicie automaticamente**

	![Inicio windows](img/11.jpg)

16. Se Guarda el estado del Servicio en **PM2**

	![Guarar Servicio](img/12.jpg)

**NOTA**  **Una vez terminado puede cerrar las ventanas con un click o con la tecla ENTER**
